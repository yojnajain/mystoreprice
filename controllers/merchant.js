var Merchant = require('../models/Merchant');
var User = require('../models/User');

var  fs = require("fs");

exports.createMerchant = function(req, res, next)
{
  Merchant.findOne({"email":req.user.email}, function(err, mer)
  {
    if(mer)
    {
      return res.render('merchant', {
        Name: 'Merchant Create', isCreate: true, merchant: mer, type:"Merchant"
      });
    }
    else
    {
      return res.render('merchant', {
        Name: 'Merchant Create', isCreate: true, merchant: {}, type:"Merchant"
      });
    }

  });
	

};
exports.postMerchant = function(req, res, next)
{

  Merchant.findOne({"email":req.user.email}, function(err, mer)
  {

  	req.assert('name', 'Name is required').notEmpty();
  	req.assert('address', 'Address is required and should be unique for your store.').notEmpty();
  	req.assert('email', 'Email is required').notEmpty();
  	req.assert('latitude', 'Enter latitude').notEmpty();
  	req.assert('longitude', 'Enter longitude').notEmpty();

  	var merchant = new Merchant();
    if(mer)
      {merchant._id = mer._id;}

    merchant.email = req.body.email.trim();
    merchant.name = req.body.name.trim();
    merchant.address = req.body.address.trim();
    merchant.about = req.body.about.trim();
    merchant.contact = req.body.contact.trim();
    merchant.latitude = req.body.latitude.trim();
    merchant.longitude = req.body.longitude.trim();
    merchant.user = req.user._id;
    var errors = req.validationErrors();

    if (errors) {
      req.flash('errors', errors);
      return res.render('merchant', {
        Name: 'Merchant Create', merchant: merchant,
        isCreate: true
      });
    }

    merchant.save(function (err, result) {
      if(result)
  	  {

  	    req.flash('success', {
  	      msg: 'Merchant details saved.'
  	    });
  	    res.redirect('/');
  	  }
    });

  });
};
exports.getMerchant = function(req, res, next)
{
  var c={}, merchant =[];
  Merchant.findById(req.params.id, function(err, mer){
  if(mer)
      { 
        c.name = mer.name;
        c.email = mer.email;
        c.address = mer.address;
        c.lat = mer.latitude;
        c.long = mer.longitude;
        c.about = mer.about;
        c.contact = mer.contact;
        c._id = mer._id; 
        merchant.push(c); 
       
        return res.send(merchant);              
      }
      if(err)
        console.log("err", err);
        return res.send("No data found");
  });
};
exports.getMerchants = function(req, res, next)
{
  var c={}, merchants =[];
  Merchant.find({}, function(err, mer){
  if(mer)
      { 
        var i=0;
        for(i=0;i<mer.length;i++)
        {
          c.name = mer[i].name;
          c.email = mer[i].email;
          c.address = mer[i].address;
          c.lat = mer[i].latitude;
          c.long = mer[i].longitude;
          c.about = mer[i].about;
          c.contact = mer[i].contact;
          c._id = mer[i]._id; 
          merchants.push(c); 
        }
        console.log("Merchants", merchants);
        return res.send(merchants);              
      }
      if(err)
        console.log("err", err);
        return res.send("No data found");
  });
};