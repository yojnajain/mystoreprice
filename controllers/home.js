var Product = require('../models/Product');
var Merchant = require('../models/Store');

exports.index = function(req, res) {
  
  Product.find({}).exec(function(err, products)
  {
      if(err)
        return req(next);
      if(products)
      {
        res.render('home', {
          title: 'Home', products: products
        });

      }
  });  

  
};   
exports.findLocation = function(req,res,next){
  var maxDistance =  5; // 8 kilometers
  maxDistance /= 6371;
  var coords = [];
  var co1 = [];
    coords[0] = req.params.long || 43.30;
    coords[1] = req.params.lat || 23.30;
     co1[0] = coords[0]+maxDistance;
    co1[1] = coords[1]+maxDistance;
    coords[0] = coords[0]-maxDistance;
    coords[1] = coords[1]-maxDistance;
   
  Merchant.find({
    'latitude':{ 
      "$gte": coords[1],
      "$lte": co1[1]
    }, 'longitude':{ 
      "$gte": coords[0],
      "$lte": co1[0]
    }
    }).exec(function(err, merchants){
    
      if (err) {
        return res.json(500, err);
      }
   
      if(merchants)
      {
        return res.json(200, merchants);
      }
  });
};
function getuserLocation() {
  if (navigator.geolocation) {
     navigator.geolocation.getCurrentPosition(showuserPosition, showbrowserError);
  }else
  {
     $('.location').text("Geolocation is not supported by this browser.");
  }
}     