var Merchant = require('../models/Merchant');
var User = require('../models/User');
var Store = require('../models/Store');

exports.getStore = function(req, res, next)
{
  Store.find({email: req.user.email}, function(err, store)
  {
    if (store)
    {
      return res.render('store', {stores:store, Name: 'Store'});
    }
    
  });

};

exports.createStore = function(req, res, next)
{
  return res.render('merchant', {
    Name: 'Store', isCreate: true, merchant: {}, type:"Store"
  });

};

exports.postStore = function(req, res, next)
{

	req.assert('name', 'Name is required').notEmpty();
	req.assert('address', 'Address is required and should be unique for your store.').notEmpty();
	//req.assert('email', 'Email is required').notEmpty();
	req.assert('latitude', 'Enter latitude').notEmpty();
	req.assert('longitude', 'Enter longitude').notEmpty();

	var store = new Store();

  store.email = req.body.email.trim();
  store.name = req.body.name.trim();
  store.address = req.body.address.trim();
  store.about = req.body.about.trim();
  store.contact = req.body.contact.trim();
  store.latitude = req.body.latitude.trim();
  store.longitude = req.body.longitude.trim();
  store.user = req.user._id;
  var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.render('store', {
      Name: 'store Create', store: store,
      isCreate: true
    });
  }

  store.save(function (err, result) {
    if(result)
	  {

	    req.flash('success', {
	      msg: 'store details saved.'
	    });
      Merchant.findOne({'email':req.user.email}, function(err, merchant)
      {
        merchant.store.push(result);
        merchant.save();
      })
	    return res.redirect('/');
	  }
  });
//return res.redirect('/');
};
