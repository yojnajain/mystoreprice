var Product = require('../models/Product');
var Merchant = require('../models/Merchant');
var User = require('../models/User');
var async = require("async");
var Store = require('../models/Store');

exports.createProduct = function(req, res, next)
{var mers = [];
  Merchant.findOne({"email":req.user.email}, function(err, mer)
  {
    if(mer)
    {
      
      return res.render('product', {
      Name: 'Product Create', isCreate: true, product: {}, stores: mer.store
      });
    }else
    {
      return res.render('product', {
      Name: 'Product Create', isCreate: true, product: {}, stores: null
      });
    }
	
  });
};
exports.postProduct = function(req, res, next)
{

	req.assert('title', 'Title is required').notEmpty();
	req.assert('desc', 'Description is required.').notEmpty();
	req.assert('mrp', 'MRP is required').notEmpty();
	req.assert('price', 'Enter Price').notEmpty();
	

	var product = new Product();

  
      var s= 0;
      if(req.body.store0)
      {
        if(req.body.store0.checked = "checked")
          product.store.push(0);
      }
      if(req.body.store1)
      { if(req.body.store1.checked = "checked")
          product.store.push(1);
      }
      if(req.body.store2)
      { if(req.body.store2.checked = "checked")
          product.store.push(2);
      }
      if(req.body.store3)
      { if(req.body.store3.checked == "checked")
          product.store.push(3);
      }
      product.title = req.body.title.trim();
      product.desc = req.body.desc.trim();
      product.mrp = req.body.mrp.trim();
      product.price = req.body.price.trim();
      product.discount = req.body.discount.trim();
      product.size = req.body.size.trim();
      product.brand = req.body.brand.trim();
      product.model = req.body.model.trim();
      product.category = req.body.category.trim();
      product.merchant = req.user._id;
      product.image = req.body.image.trim();
      
      var errors = req.validationErrors();

      if (errors) {
      req.flash('errors', errors);
      return res.render('product', {
        Name: 'Product Create', product: product,
        isCreate: true, stores:mer.store
      });
      }

      product.save(function (err, result) {
      if(result)
      {
        req.flash('success', {
          msg: 'Product details saved.'
        });
        console.log("Product", result);
        res.redirect('/');
      }
      else{
        console.log("Data not saved");
      }
      });
     

};
exports.getProducts = function(req,res,next)
{
  Product.find(function(err, result)
  {
    if(err)
    {
        console.log("Error occured " + err);
        res.send(false);
        return;
    }
    if(result) {
      var products =[];
      var c = {};
      var i=0;

      for(i=0;i<result.length;i++)
      {
        async.parallel([

          function (callback) {
          
            Merchant.find({user: c.merchant},function(err, mer)
            {   console.log(mer);
                if(mer)
                { 
                  c.merName = mer.name;
                  c.merEmail = mer.email;
                  c.merAddress = mer.address;
                  c.merLat = mer.latitude;
                  c.merLong = mer.Longitude;
                  c.merAbout = mer.about;
                  c.merContact = mer.contact;                
                }
                if(err)
                  console.log("err", err);
            });
          
          },
          function (callback){
            c.title = result[i].title;
            c.desc = result[i].desc;
            c.size = result[i].size;
            c.model = result[i].model;
            c.discount = result[i].discount;
            c.image = result[i].image;
            c.mrp = result[i].mrp;
            c.price = result[i].price;
            c.brand = result[i].brand;
            c.category = result[i].category;
            c.merchant = result[i].merchant;
          } ],
          function (err, results) {
              
          });       
      products.push(c);
              console.log();
      }   
      res.send(products);
  return;
    }

  });
  
};

