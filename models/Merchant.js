var mongoose = require('mongoose');
var User = require('./User');
var Store = require('./Store');
var merchantSchema = new mongoose.Schema({

	email: { type: String, unique: true, lowercase: true },
  name: String,
  address: String,
  latitude: Number,
  longitude: Number,
  about: String,
  contact: String,
  feedback: [{type: String}],
  created: {
    type: Date,
    default: Date.now
  },
  store: [{type: mongoose.Schema.Types.ObjectId, ref: Store}],
  user: {type: mongoose.Schema.Types.ObjectId, ref: User}


});
module.exports = mongoose.model('Merchant', merchantSchema);