var mongoose = require('mongoose');
var Merchant = require('./Merchant');
var Store = require('./Store');

var productSchema = new mongoose.Schema({

	title: String,
	desc: String,
	price: Number,
  mrp: Number,
  category: Number,
  comments: [{type:String}],
  discount: Number,
  size: Number,
  brand: String,
  model: String,
  image: String,
  merchant: {type: mongoose.Schema.Types.ObjectId, ref: Merchant},
  store: [{type:Number}],
  created: {
    type: Date,
    default: Date.now
  }

});

module.exports = mongoose.model('Product', productSchema);