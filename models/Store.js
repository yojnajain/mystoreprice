var mongoose = require('mongoose');
var User = require('./User');

var storeSchema = new mongoose.Schema({

	email: { type: String, lowercase: true },
  name: String,
  address: String,
  latitude: Number,
  longitude: Number,
  about: String,
  contact: String,
  feedback: [{type: String}],
  created: {
    type: Date,
    default: Date.now
  },
  user: {type: mongoose.Schema.Types.ObjectId, ref: User}


});
module.exports = mongoose.model('Store', storeSchema);